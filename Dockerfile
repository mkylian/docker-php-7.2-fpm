FROM php:7.2-fpm-stretch

ENV ACCEPT_EULA=Y
ENV DEBIAN_FRONTEND=noninteractive
ENV DEBIAN_LIBRARIES="\
    libbz2-dev \
    libcurl3-dev \
    libldap-dev \
    libmcrypt-dev \
    libxml2-dev \
    libmcrypt-dev \
    libssl-dev \
    libmemcached-dev \
    default-libmysqlclient-dev \
    libsqlite3-dev \
    libxml2-dev \
    libpspell-dev \
    libldap2-dev \
    unixodbc-dev \
    libpq-dev \
    libpspell-dev"
RUN apt update -qq && \
    apt install -y -qq $DEBIAN_LIBRARIES libpng-dev libjpeg-dev libzip-dev msmtp && \
    docker-php-ext-configure gd \
        --with-jpeg-dir=/usr/lib/x86_64-linux-gnu/ && \ 
    docker-php-ext-install \
        bcmath \
        bz2 \
        gd \
        intl \
        ldap \
        pdo_mysql \
        mysqli \
        opcache \
        soap \
        zip \
        pspell && \
    apt install -y gnupg2 \
    && curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
    && curl https://packages.microsoft.com/config/debian/9/prod.list \
        > /etc/apt/sources.list.d/mssql-release.list \
    && apt-get install -y --no-install-recommends \
        locales \
        apt-transport-https \
    && echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
    && locale-gen \
    && apt-get update \
    && apt-get -y --no-install-recommends install \
        unixodbc-dev \
        msodbcsql17 && \
    pecl install sqlsrv pdo_sqlsrv && \
    docker-php-ext-enable sqlsrv pdo_sqlsrv && \
    apt purge -y -qq $DEBIAN_LIBRARIES && \
    apt install -y libaspell15 && \
    apt autoremove -y && \
    rm -rf /var/lib/apt/* && \
    rm -rf /var/cache/apt/*
COPY templater /usr/local/bin/
RUN echo "TLS_REQCERT never" > /etc/ldap/ldap.conf
